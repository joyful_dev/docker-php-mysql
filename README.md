### Install
- `cp Makefile.dist Makefile`
- `cp docker-compose.yml.dist docker-compose.yml`
- Set params in `configuration` section `Makefile`
- `make init`
- `make build`

### Usage
- `make up` - start env
- `make down` - stop env
- `make cli` - for use `php` in cli, `composer` (linked with mysql)
- `make node` - for use the `npm` pakage

### DB init
Use `starting-base.sql`

OR

Do it:

```
docker run -it --rm --network=benchmark54_net -v ~/projects/php/benchmark54:/b54 mysql:5.6.33 bash

mysql -hdockerphpmysql_mysql_1 -p123 -e 'create user "b54"@"%" identified by "b54"; grant all on *.* to "b54"@"%" with grant option;flush privileges;'

mysql -hdockerphpmysql_mysql_1 -p123 -e 'CREATE DATABASE beta DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta < /b54/www/protected/data/init.txt
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta < /b54/www/protected/data/medical_procedures.sql
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta < /b54/www/protected/data/labels.sql

mysql -hdockerphpmysql_mysql_1 -p123 -e 'CREATE DATABASE test DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;'
mysql -hdockerphpmysql_mysql_1 -p123 -Dtest < /b54/www/protected/data/init.txt
mysql -hdockerphpmysql_mysql_1 -p123 -Dtest < /b54/www/protected/data/medical_procedures.sql
mysql -hdockerphpmysql_mysql_1 -p123 -Dtest < /b54/www/protected/data/labels.sql

# after migrate

mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into sport (id, name, web_css_class) values (1, "football", "football-team")'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into sport_position (id, sport_id, label, short_label, weight) values (1, 1, "Forward", "F", 1)'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into sport_position (id, sport_id, label, short_label, weight) values (2, 1, "Defender", "D", 20)'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into team(name, is_club, gender, type) values ("Test Team", 1, 0, 1)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'update team set club_id=2401,starting_sizes="a:1:{i:0;i:11;}" where id=2401';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into users (email, root_team_id, default_team_id, teams) values ("admin@example.com", 2401, 2401, "a:1:{i:0;i:2401;}");'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into users (email, root_team_id, default_team_id, teams) values ("doc@example.com", 2401, 2401, "a:1:{i:0;i:2401;}");'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into AuthAssignment (itemname, userid) values ("superadmin", 978)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into AuthAssignment (itemname, userid) values ("doctor", 979)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into seasons (title, short_title, start) values ("General Season 2015", 2015, 20150101)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into team_season (team_id, season_id) values (2401, 240)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into team_training_element (team_id, element_id) values (2401,8), (2401,13)';
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'update users set password="$2a$12$krLKL6nXYVf8Yn4nZf08DemG/0jLwzONpuSs0tqyG4a5JtdAan/Xu";'
mysql -hdockerphpmysql_mysql_1 -p123 -Dbeta -e 'insert into match_type (title) values ("test")'
```

